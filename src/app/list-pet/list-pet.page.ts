import { ViewPetPage } from './../view-pet/view-pet.page';
import { PetService } from './../shared/services/pet.service';
import { Component, OnInit } from '@angular/core';

import { PetViewModel } from '../shared/view-models/pet.view-model';
import { ActionSheetController, NavController } from '@ionic/angular';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-list-pet',
  templateUrl: './list-pet.page.html',
  styleUrls: ['./list-pet.page.scss'],
})
export class ListPetPage implements OnInit {

  public listPets: Array<PetViewModel> = [];
  public isLoading = false;

  constructor(
    private petService: PetService,
    public actionSheetController: ActionSheetController,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.getPets();
  }

  getPets() {
    this.isLoading = true;
    this.listPets = [];
    this.petService.getPets().subscribe((res) => {
      if (res) {
        res.forEach(pet => {
          this.listPets.push(new PetViewModel(pet));
        });
      }
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
      console.log(err);
    })
  }

  deletePet(petId) {
    this.isLoading = true;
    this.petService.deletePet(petId).subscribe(res => {
      if (res) {
        this.getPets();
      }
      this.isLoading = false;
    }, err => {
      this.isLoading = false;
    })
  }

  editPet(pet){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(pet)
      }
    };
    this.navCtrl.navigateForward(['/tabs/register-pet'], navigationExtras);
  }

  doRefresh(event) {
    setTimeout(() => {
      this.getPets();
      event.target.complete();
    }, 2000);
  }

  navigateToPet(row) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(row)
      }
    };
    this.navCtrl.navigateForward(['/tabs/view-pet'], navigationExtras);
  }

  async presentActionSheet(row) {
    const actionSheet = await this.actionSheetController.create({
      header: 'Ações',
      buttons: [{
        text: 'Excluir',
        role: 'destructive',
        icon: 'trash',
        handler: () => {
          this.deletePet(row.code);
        }
      },
      {
        text: 'Editar',
        icon: 'create',
        role: 'edit',
        handler: () => {
          this.editPet(row);
        }
      },
      {
        text: 'Cancel',
        icon: 'close',
        role: 'cancel',
        handler: () => { }
      }]
    });
    await actionSheet.present();
  }

}
