import { ViewPetPage } from './../view-pet/view-pet.page';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

import { ListPetPage } from '../list-pet/list-pet.page';
import { RegisterPetPage } from './../register-pet/register-pet.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'list-pet',
        component: ListPetPage
      },
      {
        path: 'view-pet',
        component: ViewPetPage
      },
      {
        path: 'register-pet',
        component: RegisterPetPage
      },
      {
        path: '',
        redirectTo: 'list-pet',
        pathMatch: 'full'
      }
    ]
  }
  
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
