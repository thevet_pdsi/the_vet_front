import { MenuToolbarComponent } from './../components/menu-toolbar/menu-toolbar.component';
import { ViewPetPage } from './../view-pet/view-pet.page';
import { RegisterPetPage } from './../register-pet/register-pet.page';
import { ListPetPage } from './../list-pet/list-pet.page';
import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs.router.module';

import { TabsPage } from './tabs.page';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule,
  ],
  declarations: [TabsPage, ListPetPage, RegisterPetPage, ViewPetPage, MenuToolbarComponent]
})
export class TabsPageModule {}
