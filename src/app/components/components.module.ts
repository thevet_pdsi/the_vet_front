import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuToolbarComponent } from './menu-toolbar/menu-toolbar.component';
import { BrowserModule } from '@angular/platform-browser';
import { IonicModule } from '@ionic/angular';

@NgModule({
  declarations: [
    MenuToolbarComponent
  ],
  exports: [
    MenuToolbarComponent
  ],
  imports: [
    CommonModule,
    IonicModule, 
  ]
})
export class ComponentsModule { }
