import { ListPetPage } from './../../list-pet/list-pet.page';
import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  public user;
  public pass;

  constructor(private navCtlr: NavController) { }

  ngOnInit() {
    this.user = 'admin',
    this.pass = 'admin'
  }

  login(){
    this.navCtlr.navigateRoot('/tabs/list-pet');
  }

}
