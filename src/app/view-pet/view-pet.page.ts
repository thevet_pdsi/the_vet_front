import { PetService } from './../shared/services/pet.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { PetViewModel } from '../shared/view-models/pet.view-model';
import { NavController } from '@ionic/angular';
import { GenderEnum } from '../shared/enums/gender.enum';

@Component({
  selector: 'app-view-pet',
  templateUrl: './view-pet.page.html',
  styleUrls: ['./view-pet.page.scss'],
})
export class ViewPetPage implements OnInit {

  public pet: PetViewModel;
  public isLoading = false;

  constructor(
    private route: ActivatedRoute,
    private petService: PetService,
    public navCtrl: NavController
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pet = new PetViewModel(JSON.parse(params.special))
        this.pet['gender_description'] = this.pet.gender === GenderEnum.female ? 'F' : 'M';
      }
    });
  }

  ngOnInit() {
  }

  deletePet() {
    this.isLoading = true;
    this.petService.deletePet(this.pet.code).subscribe(res => {
      this.isLoading = false;
      this.navCtrl.navigateRoot(['/tabs/list-pet']);
    }, err => {
      this.isLoading = false;
    })
  }

  editPet(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        special: JSON.stringify(this.pet)
      }
    };
    this.navCtrl.navigateForward(['/tabs/register-pet'], navigationExtras);
  }

}
