import { GenderEnum } from './../enums/gender.enum';
import { OwnerViewModel } from './owner.view-model';

export class PetViewModel {

   public code: string;
   public name: string;
   public birthDay: Date;
   public species: string;
   public gender: number;
   public physicalSape: string;
   public breed: string;
   public petUser: OwnerViewModel[];

   constructor(data?: any){
      this.code = data.code ? data.code : '';
      this.name = data.name ? data.name : '';
      this.birthDay = data.birthDay ? data.birthDay : '';
      this.species = data.species ? data.species : '';
      this.gender = data.gender ? data.gender : null;
      this.physicalSape = data.physicalSape ? data.physicalSape : '';
      this.breed = data.breed ? data.breed : '';
      this.petUser = data.petUser ? data.petUser : [];
   }

}