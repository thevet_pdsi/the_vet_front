export class OwnerViewModel {
   public name: string;

   constructor(data?){
      this.name = data.name ? data.name : null;
   }
}