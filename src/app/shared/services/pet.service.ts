import { PetViewModel } from './../view-models/pet.view-model';
import { Injectable } from "@angular/core";

import { HttpService } from './http.service';

@Injectable()
export class PetService {

   constructor(
      private httpService: HttpService
   ){}

   getPets(){
      const api = 'api/pets'
      return this.httpService.doGet(api);
   }

   getPetById(id: string){
      const api = 'api/pets'
      return this.httpService.doGetOne(api, id);
   }

   postPet(pet: PetViewModel){
      const api = 'api/pets'
      return this.httpService.doPost(api, pet);
   }

   putPet(pet: PetViewModel){
      const api = 'api/pets/' + pet.code
      return this.httpService.doPut(api, pet);
   }

   deletePet(petCode){
      const api = 'api/pets/' + petCode
      return this.httpService.doDelete(api);
   }

}