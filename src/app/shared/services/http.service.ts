import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders, HttpHandler } from "@angular/common/http"

@Injectable()
export class HttpService extends HttpClient{

   public url: string = 'http://localhost:60849';

   constructor(
      private http: HttpClient,
      handler: HttpHandler
   ) { 
      super(handler)
   }

   getHeaders() {
      return new HttpHeaders({ 'Content-Type': 'application/json' });
   }

   public doGet(api: string, path?: any) {
      const url = this.url + '/' + api
      const requestOptions = { headers: this.getHeaders() };
      return this.http.get<any[]>(url, requestOptions);
   }

   public doGetOne(api: string, path?: any) {
      const url = this.url + '/' + api + '/' + path;
      const requestOptions = { headers: this.getHeaders() };
      return this.http.get(url, requestOptions);
   }

   public doPost(path: string, params?: any) {
      const url = this.url + '/' + path;
      const requestOptions = { headers: this.getHeaders() };
      return this.http.post(url, params, requestOptions);
   }

   public doPut(path: string, params?: any) {
      const url = this.url + '/' + path;
      const requestOptions = { headers: this.getHeaders() };
      return this.http.put(url, params, requestOptions);
   }

   public doDelete(path: string, params?: any) {
      const url = this.url + '/' + path;
      const requestOptions = { headers: this.getHeaders() };
      return this.http.delete(url, requestOptions);
   }

}