import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { PetViewModel } from './../shared/view-models/pet.view-model';
import { OwnerViewModel } from './../shared/view-models/owner.view-model';
import { PetService } from '../shared/services/pet.service';
import { Guid } from "../shared/Utils/Guid.module";
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-register-pet',
  templateUrl: './register-pet.page.html',
  styleUrls: ['./register-pet.page.scss'],
})
export class RegisterPetPage implements OnInit {
  petForm: FormGroup;
  isSharedOwner: boolean;
  tempUsername: string;
  pet: PetViewModel;
  isEdit: boolean = false;
  btn: string = 'Cadastrar';

  constructor(
    private petService: PetService,
    private route: ActivatedRoute,
    private navCtrl: NavController
  ) {
    this.route.queryParams.subscribe(params => {
      if (params && params.special) {
        this.pet = new PetViewModel(JSON.parse(params.special))
        this.isEdit = true;
        this.btn = 'Atualizar'
      } else {
        this.pet = new PetViewModel({});
      }
    });
  }

  ngOnInit() {

  }

  get formControls() {
    return this.petForm.controls;
  }

  submitAction() {
    if (this.isEdit) {
      this.petService.putPet(this.pet).subscribe(res => {
        this.navCtrl.navigateForward(['/tabs/list-pet']);
      }, err => {
      })
    } else {
      this.pet.code = !this.pet.code ? Guid.newGuid() : this.pet.code;
      this.petService.postPet(this.pet).subscribe(res => {
        this.navCtrl.navigateForward(['/tabs/list-pet']);
      }, err => {
      })
    }

  }

  addUser() {
    this.pet.petUser = !this.pet.petUser ? [] : this.pet.petUser
    var user = new OwnerViewModel({});
    user.name = this.tempUsername;
    this.pet.petUser.push(user);
  }

  deleteOwner(owner: OwnerViewModel) {
    var index = this.pet.petUser.indexOf(owner);
    this.pet.petUser.splice(index, 1);
  }

}